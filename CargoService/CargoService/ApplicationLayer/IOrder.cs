﻿using System;
using System.Collections.Generic;
using System.Linq;
using CargoService.DomainLayer;
using CargoService.Windsor;

namespace CargoService.ApplicationLayer
{
    internal interface IOrder
    {
        Order CreateOrder();
        void ChangeOrderStatus(Order order, string newStatus);
        void ChangeOrderData(int id);
        OrderStatus GetOrderStatus(Order order);
    }

    internal class OrderImplement : IOrder
    {
        public void ChangeOrderData(int id)
        {
            var db = Program.Db;
            foreach (var order in db.Orders.ToList().Where(order => order.Id == id))
            {
                var stop = false;
                if (order.Status.Status != "Init")
                {
                    Console.WriteLine("you can only edit orders with init status");
                    stop = true;
                }

                while (!stop)
                {
                    Console.WriteLine("Input operation[d=delete cargo|a=add cargo|e=exit]");
                    Console.Write(">");
                    var op = Console.ReadLine();
                    if (op == "e")
                    {
                        stop = true;
                    }
                    if (op == "d")
                    {
                        PrintCargosList(order);
                        var cId = int.Parse(GetString("input cargo id to delete"));
                        foreach (var cargo in order.Cargos)
                        {
                            var cargoDeleted = false;
                            if (cId == cargo.Id)
                            {
                                var cargo1 = cargo;
                                var cargo2 = cargo;
                                foreach (
                                    var usedVehicle in
                                        order.UsedVehicles.Where(
                                            usedVehicle =>
                                                cargo2.CargoType.SupportedVehicleTypes.Any(
                                                    supportedVehicleType =>
                                                        supportedVehicleType == usedVehicle.Type &&
                                                         cargo1.Weight <=
                                                        usedVehicle.initMaxVeight - usedVehicle.MaxWeight)))
                                {
                                    usedVehicle.MaxWeight += cargo.Weight;
                                    if (usedVehicle.MaxWeight == usedVehicle.initMaxVeight)
                                    {
                                        usedVehicle.avalibility = true;
                                        order.UsedVehicles.Remove(usedVehicle);
                                    }
                                    order.Cargos.Remove(cargo);
                                    cargoDeleted = true;
                                    break;
                                }
                            }
                            if (cargoDeleted)
                                break;
                        }
                        db.SaveChanges();
                    }
                    if (op != "a") continue;
                    foreach (var vehicle in from usedVehicle in order.UsedVehicles
                        from vehicle in db.Vehicles
                        where vehicle.Number == usedVehicle.Number
                        select vehicle)
                    {
                        vehicle.avalibility = true;
                    }
                    db.SaveChanges();

                    var av = db.Vehicles.Where(vehicle => vehicle.avalibility).ToList();
                    AddCargo(order.Cargos, av, db.CargoTypes.ToList(), order);
                    foreach (var vehicle in from usedVehicle in order.UsedVehicles
                        from vehicle in db.Vehicles
                        where vehicle.Number == usedVehicle.Number
                        select vehicle)
                    {
                        vehicle.avalibility = false;
                    }
                    db.SaveChanges();
                }
            }
        }

        private static void PrintCargosList(Order order)
        {
            foreach (var cargo in order.Cargos)
            {
                Console.WriteLine("id {0}: type = {1}\tweight = {2}",cargo.Id, cargo.CargoType.Type, cargo.Weight);
            }
        }
        private static string GetString(string name)
        {
            Console.WriteLine("Enter {0}", name);
            var input = Console.ReadLine();
            return input;
        }

        private static void AddCargo(ICollection<Cargo> cargos, List<Vehicle> availibleVehicles, List<CargoType> types,
            Order order)
        {
            Program.ShowVehiclesList();

            var cargo = new Cargo
            {
                Id = int.Parse(GetString("id")),
                Weight = int.Parse(GetString("weight"))
            };

            var search = GetString("type");
            cargo.CargoType = types.Find(x => x.Type.Contains(search));
            if (cargo.CargoType == null)
            {
                Console.WriteLine("Invalid cargo type");
                return;
            }

            foreach (var supportedVehicleType in cargo.CargoType.SupportedVehicleTypes)
            {
                Vehicle selectedVehicle;
                var checkAvailiblVehicle =
                    availibleVehicles.Where(availibleVehicle => supportedVehicleType.Type == availibleVehicle.Type.Type)
                        .ToList();
                if (checkAvailiblVehicle.Count == 0)
                {
                    Console.WriteLine("No vehicles avalible for this cargo(type)");
                    continue;
                }


                foreach (var vehicle in checkAvailiblVehicle.Where(vehicle => cargo.Weight <= vehicle.MaxWeight))
                {
                    selectedVehicle = vehicle;
                    cargos.Add(cargo);
                    Program.Db.Cargos.Add(cargo);

                    var root = Program.Container.Resolve<ICompositionRoot>();
                    root.LogMessage(DateTime.Now + ": Cargo, type = " + cargo.CargoType.Type + " weight = " +
                                    cargo.Weight + " added");

                    try
                    {
                        foreach (
                            var availibleVehicle in
                                availibleVehicles.Where(availibleVehicle => availibleVehicle.Id == selectedVehicle.Id))
                        {
                            availibleVehicle.MaxWeight -= cargo.Weight;
                            order.UsedVehicles.Add(selectedVehicle);
                        }
                    }
                    catch (InvalidOperationException)
                    {
                    }
                    return;
                }
                Console.WriteLine("No vehicles avalible for this cargo(max weight)");
                return;
            }
        }

        public void ChangeOrderStatus(Order order, string newStatus)
        {
            var db = Program.Db;
            var root = Program.Container.Resolve<ICompositionRoot>();

            if (newStatus == "p")
            {
                if (order.Status.Status != "Init")
                {
                    Console.WriteLine("unable to swap to this status, current order status must be init");
                    return;
                }
                order.Status.Status = "processing";

                root.LogMessage(DateTime.Now + " - Order" + order.Id + "changed");
            }
            if (newStatus == "c")
            {
                if (order.Status.Status != "processing")
                {
                    Console.WriteLine("unable to swap to this status, current order ststus must be processing");
                    return;
                }
                foreach (var vehicle in from usedVehicle in order.UsedVehicles from vehicle in db.Vehicles where vehicle.Number == usedVehicle.Number select vehicle)
                {
                    vehicle.avalibility = true;
                    vehicle.MaxWeight = vehicle.initMaxVeight;
                }
                Program.CompleteedOrders.Add(order);
                order.Status.Status = "completed";
                root.LogMessage(DateTime.Now + ": Order" + order.Id + "changed");
            }
            if (newStatus == "ca")
            {
                if (order.Status.Status != "Init")
                {
                    Console.WriteLine("unable to swap to this status, current order status must be init.");
                    return;
                }
                foreach (var vehicle in from usedVehicle in order.UsedVehicles from vehicle in db.Vehicles where vehicle.Number == usedVehicle.Number select vehicle)
                {
                    vehicle.avalibility = true;
                    vehicle.MaxWeight = vehicle.initMaxVeight;
                }
                order.Status.Status = "canceled";
                root.LogMessage(DateTime.Now + ": Order" + order.Id + "changed");
                db.SaveChanges();
            }
            db.SaveChanges();
        }

        public OrderStatus GetOrderStatus(Order order)
        {
            return order.Status;
        }

        public Order CreateOrder()
        {
            var db = Program.Db;
            db.Database.CreateIfNotExists();
            var order = new Order();
            //var test = new List<VehicleType>(db.VehicleTypes);
            var vehicles = new List<Vehicle>(db.Vehicles);
            var aVehicles = vehicles.Where(vehicle => vehicle.avalibility).ToList();

            order.UsedVehicles = new List<Vehicle>();

            Console.WriteLine("\n---Input Client info---");
            var client = new Client
            {
                Name = GetString("name"),
                Document = GetString("document"),
                Contacts = GetString("contacts")
            };
            db.Clients.Add(client);
            db.SaveChanges();
            foreach (var client1 in db.Clients.Where(client1 => client1.Id == client.Id))
            {
                order.Client = client1;
            }
           

            Console.WriteLine("\n---Input Admin info---");
            var admin = new Administrator
            {
                Id = int.Parse(GetString("id")),
                Name = GetString("name")
            };
            db.Administrators.Add(admin);
            db.SaveChanges();
            foreach (var administrator in db.Administrators)
            {
                if(administrator.Id == admin.Id)
                    order.Administrator = administrator;
            }

            var cargos = new List<Cargo>();
            Console.WriteLine("\n---Adding Cargos---");

            var stop = false;
            while (!stop)
            {
                AddCargo(cargos, aVehicles, Program.Db.CargoTypes.ToList(), order);

                Console.WriteLine("type + to add another cargo");
                var key = Console.ReadLine();
                if (key != "+")
                    stop = true;
            }

            order.Cargos = cargos;
            order.CalculateCost();
            order.Status = new OrderStatus(order.Id, "Init", int.Parse(GetString("Time limit")), DateTime.Now);

            foreach (var usedVehicle in order.UsedVehicles)
            {
                usedVehicle.avalibility = false;
            }

            db.SaveChanges();
            return order;

        }
    }
}

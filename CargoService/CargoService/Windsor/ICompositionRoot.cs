﻿using System;

namespace CargoService.Windsor
{
    interface ICompositionRoot
    {
        void LogMessage(string message);
    }

    public interface IConsoleWriter
    {
        void LogMessage(string message);
    }

    public class ConsoleWriter : IConsoleWriter
    {
        public void LogMessage(string message)
        {
            Console.WriteLine("log updated");
            Program.Log.Add(message);
        }
    }

}

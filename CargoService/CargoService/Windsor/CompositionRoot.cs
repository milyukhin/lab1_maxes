﻿//  --------------------------------------------------------------------------------------
// DependencyInjection.GettingStarted.CompositionRoot.cs
// 2017/01/11
//  --------------------------------------------------------------------------------------

namespace CargoService.Windsor
{
        public class CompositionRoot : ICompositionRoot
        {
            readonly IConsoleWriter _consoleWriter;

            public CompositionRoot(IConsoleWriter consoleWriter)
            {
                _consoleWriter = consoleWriter;
                consoleWriter.LogMessage("CompositionRoot Constructor used!");
            }

            public void LogMessage(string message)
            {
                _consoleWriter.LogMessage(message);
            }
        }
    
}
﻿namespace CargoService.DomainLayer
{
    internal class Client
    {
       public int Id{ get; set;}
       public string Name{ get; set;}
       public string Document { get;  set; }
       public string Contacts { get;  set; }
       public Client() { }
    }
}

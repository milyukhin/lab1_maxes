﻿namespace CargoService.DomainLayer
{
    internal class Administrator
    {
        public int Id { get;  set; }
        public string Name { get;  set; }
    }
}

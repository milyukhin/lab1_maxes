﻿using System;

namespace CargoService.DomainLayer
{
    internal class OrderStatus
    {
        public OrderStatus(int id, string status, int timeLimit, DateTime acceptDate)
        {
            Id = id;
            Status = status;
            TimeLimit = timeLimit;
            AcceptDate = acceptDate;
        }
        public OrderStatus() { }

        public int Id { get; set; }
        public string Status { get;  set; }
        public int TimeLimit { get; private set; }
        public DateTime AcceptDate { get; private set; }
    }
}

﻿namespace CargoService.DomainLayer
{
    internal sealed class Cargo
    {
        public int Id { get;  set; }
        public CargoType CargoType { get; set; }
        public int Weight { get;  set; }
    }
}

﻿namespace CargoService.DomainLayer
{
    internal class VehicleType
    {
        public int Id { get;  set; }
        public string Type { get;  set; }

        public VehicleType() { }
        public VehicleType(string type)
        {
            Type = type;
        }
    }
}

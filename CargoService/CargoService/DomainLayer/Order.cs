﻿using System;
using System.Collections.Generic;

namespace CargoService.DomainLayer
{
    internal sealed class Order
    {
        public int Id { get;  set; }
        public Client Client { get;  set; }
        public Administrator Administrator { get; set; }
        public List<Cargo> Cargos { get; set; }
        public List<Vehicle> UsedVehicles { get; set; }
        public OrderStatus Status { get; set; }
        public int Cost { get; private set; }

        public void CalculateCost()
        {
            Console.WriteLine("Type orders distance");
            var distance = 0;
            var stop = false;
            while (!stop)
            {
                stop = int.TryParse(Console.ReadLine(), out distance);
            }
            Cost = distance*UsedVehicles.Capacity;
        }
    }
}

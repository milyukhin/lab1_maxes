﻿namespace CargoService.DomainLayer
{
    internal sealed class Vehicle
    {
        public int Id { get; private set; }
        public string Number { get; private set; }
        public VehicleType Type { get; private set; }
        public int MaxWeight { get;  set; }
        public bool avalibility { get;  set; }
        public int initMaxVeight { get;  set; }

        public Vehicle(){}
        public Vehicle(int id,string number,VehicleType type,int mv)
        {
            Id = id; Number = number; Type = type; MaxWeight = mv;
            initMaxVeight = mv;
            avalibility = true;
        }
    }
}

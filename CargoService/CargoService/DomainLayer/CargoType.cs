﻿using System.Collections.Generic;

namespace CargoService.DomainLayer
{
    internal class CargoType
    {
        public int Id { get; set; }
        public string Type { get; private set; }
        public virtual List<VehicleType> SupportedVehicleTypes { get; private set; }

        public CargoType(int id, string type, List<VehicleType> supportedTypes)
        {
            Id = id;
            Type = type;
            SupportedVehicleTypes = supportedTypes;
        }
        public CargoType() { }
    }
}

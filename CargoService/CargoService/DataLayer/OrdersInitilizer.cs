﻿using System.Collections.Generic;
using System.Data.Entity;
using CargoService.DomainLayer;

namespace CargoService.DataLayer
{
    internal class OrdersInitilizer : DropCreateDatabaseIfModelChanges<OrdersContext>
    //internal class OrdersInitilizer : DropCreateDatabaseAlways<OrdersContext>
    {
        protected override void Seed(OrdersContext context)
        {
            var vehicleTypes = new List<VehicleType>
            {
                new VehicleType("solid"),
                new VehicleType("liquid"),
                new VehicleType("fragile")
            };
            vehicleTypes.ForEach(s => context.VehicleTypes.Add(s));
            context.SaveChanges();

            var vehicles = new List<Vehicle>
            {
                new Vehicle(1, "1", context.VehicleTypes.Find(1), 5000),
                new Vehicle(2, "2", context.VehicleTypes.Find(1), 3000),
                new Vehicle(3, "3", context.VehicleTypes.Find(2), 2000),
                new Vehicle(4, "4", context.VehicleTypes.Find(2), 1500),
                new Vehicle(5, "5", context.VehicleTypes.Find(3), 1000)
            };
            vehicles.ForEach(s => context.Vehicles.Add(s));
            context.SaveChanges();

            var cargoTypes = new List<CargoType>
            {
                new CargoType(1, "basic", new List<VehicleType>() {vehicleTypes[0], vehicleTypes[2]}),
                new CargoType(2, "liquid", new List<VehicleType>() {vehicleTypes[1]}),
                new CargoType(3, "fragile", new List<VehicleType>() {vehicleTypes[2]})
            };
            cargoTypes.ForEach(s => context.CargoTypes.Add(s));
            context.SaveChanges();
        }

    }
}
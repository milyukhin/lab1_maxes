﻿using System.Data.Entity;
using CargoService.DomainLayer;

namespace CargoService.DataLayer
{
    internal class OrdersContext : DbContext
    {
        static OrdersContext() 
        {
            Database.SetInitializer(new OrdersInitilizer());
        }
        public OrdersContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer(new OrdersInitilizer());
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        //public DbSet<Vehicle> AvalibleVehicles { get; set; }
        public DbSet<VehicleType> VehicleTypes { get; set; }
        public DbSet<CargoType> CargoTypes { get; set; }
        public DbSet<Cargo> Cargos { get; set; }
        public DbSet<Order> Orders { get; set; }

        /*protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }*/
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using CargoService.ApplicationLayer;
using CargoService.DataLayer;
using CargoService.DomainLayer;
using CargoService.Windsor;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace CargoService
{
    internal static class Program
    {
        public static readonly List<Order> CompleteedOrders = new List<Order>();
        public static List<string> Log = new List<string>();
        public static WindsorContainer Container;
        public static readonly OrdersContext Db = new OrdersContext();

        private static void DbInitCostyl()
        {
            var clients = Db.Clients.ToList();
            var a = Db.Administrators.ToList();
            var orders = Db.Orders.ToList();
            var c = Db.Cargos.ToList();
            var v = Db.Vehicles.ToList();
            var vt = Db.VehicleTypes.ToList();
            var ct = Db.CargoTypes.ToList();
            var os = Db.OrderStatuses.ToList();
        }

        private static void Main()
        {
            System.Data.Entity.Database.SetInitializer(new OrdersInitilizer());
            Container = new WindsorContainer();
            DbInitCostyl();

            // Register the CompositionRoot type with the Container
            Container.Register(Component.For<ICompositionRoot>().ImplementedBy<CompositionRoot>());
            Container.Register(Component.For<IConsoleWriter>().ImplementedBy<ConsoleWriter>());

            #region 123

            // Resolve an object of type ICompositionRoot (ask the Container for an instance)
            // This is analagous to calling new() in a non-IoC application.
            Container.Resolve<ICompositionRoot>();
            /*
            VehicleTypes.Add(new VehicleType(1, "solid"));
            VehicleTypes.Add(new VehicleType(2, "liquid"));
            VehicleTypes.Add(new VehicleType(3, "fragile"));

            Vehicles.Add(new Vehicle(1, "1", VehicleTypes[0], 5000));
            Vehicles.Add(new Vehicle(2, "2", VehicleTypes[0], 3000));
            Vehicles.Add(new Vehicle(3, "3", VehicleTypes[1], 2000));
            Vehicles.Add(new Vehicle(4, "4", VehicleTypes[1], 1500));
            Vehicles.Add(new Vehicle(5, "5", VehicleTypes[2], 1000));
            foreach (var v in Vehicles)
                AvalibleVehicles.Add(new Vehicle(v.Id,v.Number,v.Type,v.MaxWeight));
                       
            CargoTypes.Add(new CargoType(1, "basic", new List<VehicleType>() { VehicleTypes[0],VehicleTypes[2] }));
            CargoTypes.Add(new CargoType(2, "liquid", new List<VehicleType>() { VehicleTypes[1] }));
            CargoTypes.Add(new CargoType(3, "fragile", new List<VehicleType>() { VehicleTypes[2] }));
            */

            //Db.VehicleTypes.Add(new VehicleType(1, "solid"));
            /* Db.VehicleTypes.Add(new VehicleType(2, "liquid"));
            Db.VehicleTypes.Add(new VehicleType(3, "fragile"));

            Db.Vehicles.Add(new Vehicle(1, "1", Db.VehicleTypes.Find(1), 5000));
            Db.Vehicles.Add(new Vehicle(2, "2", Db.VehicleTypes.Find(1), 3000));
            Db.Vehicles.Add(new Vehicle(3, "3", Db.VehicleTypes.Find(2), 2000));
            Db.Vehicles.Add(new Vehicle(4, "4", Db.VehicleTypes.Find(2), 1500));
            Db.Vehicles.Add(new Vehicle(5, "5", Db.VehicleTypes.Find(3), 1000));

            Db.CargoTypes.Add(new CargoType(1, "basic",
                new List<VehicleType>() {Db.VehicleTypes.Find(1), Db.VehicleTypes.Find(3)}));
            Db.CargoTypes.Add(new CargoType(2, "liquid", new List<VehicleType>() {Db.VehicleTypes.Find(2)}));
            Db.CargoTypes.Add(new CargoType(3, "fragile", new List<VehicleType>() {Db.VehicleTypes.Find(3)}));
            Db.SaveChanges();*/

            #endregion

            var command = "-";
            while (command != "exit")
            {
                Console.WriteLine(
                    "-------------------------------------------\nCommands\nadd - add order\nedit - edit order status\neditO - edit order data\nprint - print all Orders\nprintO - print full order info\nexit - quit program\nlog - print log");
                Console.Write(">");
                command = Console.ReadLine();
                if (command == "add")
                    AddOrder();
                if (command == "print")
                    PrintOrdersList();
                if (command == "printO")
                    PrintOrder();
                if (command == "edit")
                    EditOrder();
                if (command == "log")
                    PrintLog();
                if (command == "editO")
                    EditOrderData();
                Console.WriteLine();
            }
        }

        private static void PrintLog()
        {
            Console.WriteLine("-----LOG-----");
            foreach (var entry in Log)
            {
                Console.WriteLine(entry);
            }
            Console.WriteLine();
        }

        public static void ShowVehiclesList()
        {
            Console.WriteLine("Availible vehicles:");
            Console.WriteLine("Id\tNumber\tType\tMaxVeight");

            foreach (var vehicle in Db.Vehicles.Where(vehicle => vehicle.avalibility))
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", vehicle.Id, vehicle.Number, vehicle.Type.Type,
                    vehicle.MaxWeight);
            }
            Console.WriteLine();
        }
        
        private static void AddOrder()
        {
            var impl = new OrderImplement();

            var order = impl.CreateOrder();
            if (order == null) return;

            Db.Orders.Add(order);
            var root = Container.Resolve<ICompositionRoot>();
            root.LogMessage(DateTime.Now + ": Order,id= " + order.Id + " added");
            Db.SaveChanges();
        }

        private static void PrintOrder()
        {
            PrintOrdersList();
            Console.WriteLine("type order id");
            var id = int.Parse(Console.ReadLine());
            foreach (var order in Db.Orders.Where(order => id == order.Id))
            {
                Console.WriteLine("Client:");
                Console.WriteLine("name: {0}\tdocument: {2}\tcontact: {1}", order.Client.Name, order.Client.Contacts,
                    order.Client.Document);
                Console.WriteLine("Admin:");
                Console.WriteLine("name: {0}", order.Administrator.Name);
                Console.WriteLine("Cargos:");
                foreach (var cargo in order.Cargos)
                {
                    Console.WriteLine("type: {0}\tweight: {1}", cargo.CargoType.Type, cargo.Weight);
                }
                Console.WriteLine("Vehicles:");
                if (order.UsedVehicles != null)
                    foreach (var cargo in order.UsedVehicles)
                    {
                        Console.WriteLine("number: {0}\ttype: {1}\tmax Weight={2}", cargo.Number, cargo.Type.Type,
                            cargo.MaxWeight);
                    }
                Console.WriteLine("Status info:");
                Console.WriteLine("Status: {0}\tTime limit = {1}\tCost = {2}\tAccept Date: {3}", order.Status.Status,
                    order.Status.TimeLimit, order.Cost, order.Status.AcceptDate);
                return;
            }
        }

        private static void PrintOrdersList()
        {
            Console.WriteLine("Orders\nId\tClient\tStatus\tCargos\tVehicles");


            foreach (var order in Db.Orders)
            {
                var name = order.Client.Name ?? "-";
                var status = order.Status.Status ?? "UNKNOWN";
                var cc = "-";
                if (order.Cargos != null)
                    cc = order.Cargos.Count.ToString();
                var uv = "0";
                if (order.UsedVehicles != null)
                    uv = order.UsedVehicles.Count.ToString();


                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", order.Id, name, status, cc, uv);
            }
        }
        private static void EditOrder()
        {
            var proc = new OrderImplement();
            PrintOrdersList();
            Console.WriteLine("Enter order id"); Console.Write(">");
            var id = int.Parse(Console.ReadLine());
            foreach (var order in Db.Orders.ToList().Where(order => order.Id == id))
            {
                Console.WriteLine("Input new status[p=processing|c=completed|ca=cancelled]"); Console.Write(">");
                proc.ChangeOrderStatus(order, Console.ReadLine());
            }
        }

        private static void EditOrderData()
        {
            var proc = new OrderImplement();
            PrintOrdersList();
            Console.WriteLine("Enter order id"); Console.Write(">");
            var id = int.Parse(Console.ReadLine());
            proc.ChangeOrderData(id);
        }
    }
}
